provider "aws" {
  region     = "eu-west-2"
}

resource "aws_instance" "web" {
  ami           = "ami-524e5736"
  instance_type = "t2.micro"
}